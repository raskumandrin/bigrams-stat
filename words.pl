#!/usr/bin/perl
use strict;
use feature 'say';

my %term_frequency;
my %document_frequency;
my $rows = 0;
while (<>) {
	chomp;
	my $word_line = ( split /\t/ )[5]; # get 6th column
	next unless $word_line;

	$rows++;
	
	my %row_unique_terms;
	
	foreach my $term ( split ',',$word_line ) {
		$term_frequency{$term}++;
		$row_unique_terms{$term} = undef;
	}
	
	# for each unique word in row increase row counter
	$document_frequency{$_}++ foreach keys %row_unique_terms;
	
	%row_unique_terms = ();
	$word_line = undef;
}

# output

# total number of all words
my $total = scalar( keys %term_frequency );

foreach my $term ( keys %term_frequency ) {
	say "$term\t$term_frequency{$term}\t"
		. sprintf("%.4f", $term_frequency{$term} / $total)
		. "\t$document_frequency{$term}\t"
		. sprintf("%.4f", $document_frequency{$term} / $rows);
}