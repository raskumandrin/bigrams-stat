#!/usr/bin/perl
use strict;
use feature 'say';

my %gram_frequency;
my %document_frequency;
my $rows = 0;
while (<>) {
	chomp;
	my $word_line = ( split /\t/ )[5]; # get 6th column
	next unless $word_line;

	my @word_line = split ',',$word_line;

	next if scalar(@word_line) < 2;

	$rows++;
	my %row_unique_grams;

	foreach my $i (0..(scalar(@word_line)-2)) {
		my $bigram = $word_line[$i].','.$word_line[$i+1];
		$gram_frequency{$bigram}++;
		$row_unique_grams{$bigram} = undef;
		$bigram = undef;
	}

	# for each unique bigram in row increase row counter
	$document_frequency{$_}++ foreach keys %row_unique_grams;
	
	%row_unique_grams = ();
	$word_line = undef;
	@word_line = ();
}

# output

# total number of all words
my $total = scalar( keys %gram_frequency );

foreach my $bigram ( keys %gram_frequency ) {
	say "$bigram\t$gram_frequency{$bigram}\t"
		. sprintf("%.4f", $gram_frequency{$bigram} / $total)
		. "\t$document_frequency{$bigram}\t"
		. sprintf("%.4f", $document_frequency{$bigram} / $rows);
}